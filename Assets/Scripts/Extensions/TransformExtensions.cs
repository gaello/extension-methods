﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class contains extensions to the Transform class.
/// </summary>
public static class TransformExtensions
{
    /// <summary>
    /// Gets all children of tranfsorm and returns them.
    /// </summary>
    /// <returns>List of childern.</returns>
    /// <param name="transform">Transform.</param>
    public static List<Transform> GetChildren(this Transform transform)
    {
        var children = new List<Transform>();

        // Iterate over all children in transform.
        foreach (Transform child in transform)
        {
            children.Add(child);
        }

        return children;
    }
}
