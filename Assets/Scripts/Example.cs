﻿using UnityEngine;

/// <summary>
/// Example of use for class extensions.
/// </summary>
public class Example : MonoBehaviour
{
    /// <summary>
    /// Unity method called on the first frame of the game.
    /// </summary>
    void Start()
    {
        var children = transform.GetChildren();

        foreach (var child in children)
        {
            child.name = name.AddRandomText(10);
        }
    }
}
